# CallogItFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Example de structure projet

https://github.com/johnpapa/angular-first-look-examples/tree/master/_examples/storyline-tracker/app

## Angular Router Series: Secondary Outlets Primer

https://blog.angularindepth.com/angular-router-series-secondary-outlets-primer-139206595e2

## A faire :

https://www.tektutorialshub.com/angular/angular-how-to-use-app-initializer/
https://medium.com/better-programming/cleaner-typescript-with-the-non-null-assertion-operator-300789388376
https://monpetitdev.fr/angular-datepicker-material/
https://malcoded.com/posts/angular-dynamic-components/

#About change detection
https://blog.angularindepth.com/these-5-articles-will-make-you-an-angular-change-detection-expert-ed530d28930

Voir aussi mon compte
https://stackblitz.com/edit/angular-4kaaet?file=src/app/app.component.ts
https://blog.angularindepth.com/a-gentle-introduction-into-change-detection-in-angular-33f9ffff6f10

#Plugins for vscode
https://johnpapa.net/rec-ng-extensions/

#NGRX 8
https://medium.com/ngrx/announcing-ngrx-version-8-ngrx-data-create-functions-runtime-checks-and-mock-selectors-a44fac112627
https://brianflove.com/2018/06/28/ngrx-testing-effects/
https://dev.to/fallenstedt/testing-ngrx-effects-2mcl
