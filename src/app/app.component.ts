import { Component, OnInit } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { ExampleComponent } from './shared/dialog/components/example/example.component';
import { DialogService } from './shared/dialog/dialog.service';
import { FakeService } from './shared/services/fake.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'callog-it-front';

  constructor(
    private fakeService: FakeService,
    public dialog: DialogService
  ) { }

  ngOnInit(): void {
    this.fakeService.add();
    console.log('[root] AppComponent this.fakeService.getCount() =>', this.fakeService.getCount());
  }

  openModal() {
    const ref = this.dialog.open(ExampleComponent, {
      data: { message: 'I am a dynamic component inside of a dialog!' },
    });

    ref.afterClosed
      .pipe(
        tap(result => {
          console.log('Dialog closed', result);
        }),
        take(1)
      ).subscribe();
  }
}
