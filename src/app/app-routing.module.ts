import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';

export const routesPluralsight = {
  pluralsight_angular_routing: 'pluralsight/angular-routing',
};

export const routesAngularUniversity = {
  angular_university_angular_testing: 'angular_university/angular-testing',
  angular_university_angular_ngrx: 'angular_university/angular-ngrx',
};

export const routesMedium = {
  medium_angular: 'medium',
};

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent

  },
  {
    path: routesPluralsight.pluralsight_angular_routing,
    loadChildren: () => import('./features-modules/pluralsight/angular-routing-course/angular-routing-course.module')
      .then(m => m.AngularRoutingCourseModule)
  },
  {
    path: routesAngularUniversity.angular_university_angular_testing,
    loadChildren: () => import('./features-modules/angular-university/angular-testing-course/angular-testing-course.module')
      .then(m => m.AngularTestingCourseModule)
  },
  {
    path: routesAngularUniversity.angular_university_angular_ngrx,
    loadChildren: () => import('./features-modules/angular-university/ngrx-course/angular-ngrx-course.module')
      .then(m => m.AngularNgrxCourseModule)
  },
  {
    path: routesMedium.medium_angular,
    loadChildren: () => import('./features-modules/medium/medium.module')
      .then(m => m.MediumModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: false }) // <-- debugging purposes only)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
  static components = [
    AppComponent,
    HomeComponent
  ];
}
