import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AngularRoutingCourseRoutingModule } from './angular-routing-course-routing.module';
import { MessageModule } from './app-source/messages/message.module';
import { ProductModule } from './app-source/products/product.module';
import { UserModule } from './app-source/user/user.module';

@NgModule({
  declarations: [
    ...AngularRoutingCourseRoutingModule.components
  ],
  imports: [
    CommonModule,
    AngularRoutingCourseRoutingModule,
    ProductModule,
    UserModule,
    MessageModule,

  ]
})
export class AngularRoutingCourseModule { }
