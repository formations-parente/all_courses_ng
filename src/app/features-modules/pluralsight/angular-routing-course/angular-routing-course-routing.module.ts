import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routesPluralsight } from 'src/app/app-routing.module';

import { AppRoutingCourseComponent } from './app-source/app.component';
import { WelcomeComponent } from './app-source/home/welcome.component';


const routes: Routes = [
    {
        path: '',
        component: AppRoutingCourseComponent,
        children: [
            {
                path: '',
                redirectTo: `/${routesPluralsight.pluralsight_angular_routing}/welcome`,
                pathMatch: 'full'
            },
            {
                path: 'welcome',
                component: WelcomeComponent,
            },
            {
                path: 'products',
                loadChildren: () => import('./app-source/products/product.module')
                    .then(m => m.ProductModule)
            },
            {
                path: 'login',
                loadChildren: () => import('./app-source/user/user.module')
                    .then(m => m.UserModule)
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AngularRoutingCourseRoutingModule {
    static components = [
        AppRoutingCourseComponent,
        WelcomeComponent
    ];
}
