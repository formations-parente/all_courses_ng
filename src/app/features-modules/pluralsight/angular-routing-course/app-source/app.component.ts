import { Component } from '@angular/core';

import { AuthService } from './user/auth.service';
import { Router } from '@angular/router';
import { routesPluralsight } from 'src/app/app-routing.module';

@Component({
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppRoutingCourseComponent {
  pageTitle = 'Acme Product Management';

  get isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

  get userName(): string {
    if (this.authService.currentUser) {
      return this.authService.currentUser.userName;
    }
    return '';
  }

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  logOut(): void {
    this.authService.logout();
    this.router.navigate([`${routesPluralsight.pluralsight_angular_routing}/welcome`]);
    // console.log('Log out');
  }
}
