import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './product-edit-info.component.html'
})
export class ProductEditInfoComponent {
  @ViewChild(NgForm, { static: false }) productForm: NgForm;

  errorMessage: string;
  product = { id: 1, productName: 'test', productCode: 'test', description: 'test' };
}
