import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routesPluralsight } from 'src/app/app-routing.module';

import { AppComponent } from './app-source/app.component';
import { HomeComponent } from './app-source/courses/home/home.component';
import { AboutComponent } from './app-source/about/about.component';
import { CourseComponent } from './app-source/courses/course/course.component';
import { CourseResolver } from './app-source/courses/services/course.resolver';

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: '',
                component: HomeComponent

            },
            {
                path: 'about',
                component: AboutComponent
            },
            {
                path: 'courses/:id',
                component: CourseComponent,
                resolve: {
                    course: CourseResolver
                }
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AngularTestingCourseRoutingModule {
    static components = [
        AppComponent,
    ];
}
