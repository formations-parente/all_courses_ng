import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { AngularTestingCourseRoutingModule } from './angular-testing-course-routing.module';
import { CoursesModule } from './app-source/courses/courses.module';

@NgModule({
  declarations: [
    ...AngularTestingCourseRoutingModule.components
  ],
  imports: [
    SharedModule,
    CoursesModule,
    AngularTestingCourseRoutingModule,

  ]
})
export class AngularTestingCourseModule { }
