import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app-source/app.component';
import { AuthGuard } from './app-source/auth/guards/auth.guard';


const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            },
            {
                path: 'login',
                loadChildren: () => import('./app-source/auth/auth.module')
                    .then(m => m.AuthModule)
            },
            {
                path: 'courses',
                loadChildren: () => import('./app-source/courses/courses.module')
                    .then(m => m.CoursesModule),
                canActivate: [AuthGuard]

            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AngularNgrxCourseRoutingModule {
    static components = [
        AppComponent
    ];
}
