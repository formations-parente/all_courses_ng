import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { noop } from 'rxjs';
import { tap } from 'rxjs/operators';
import { routesAngularUniversity } from 'src/app/app-routing.module';

import { AuthService } from '../auth.service';
import { User } from '../model/user.model';
import * as fromAuthFeature from '../store';
import { AuthState } from '../store/auth.reducers';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private store: Store<AuthState>
  ) {

    this.form = fb.group({
      email: ['test@angular-university.io', [Validators.required]],
      password: ['test', [Validators.required]]
    });

  }

  ngOnInit() {

  }

  login() {
    const { email, password } = this.form.value as User;
    this.auth.login(email, password)
      .pipe(
        tap(user => {
          console.log(user);
          fromAuthFeature.AuthDispatchers.dispatchActionLogin(this.store, user);
          this.router.navigate([`${routesAngularUniversity.angular_university_angular_ngrx}/courses`]);
        })
      )
      .subscribe(
        noop,
        () => alert('Login failed...')
      );
  }

}

