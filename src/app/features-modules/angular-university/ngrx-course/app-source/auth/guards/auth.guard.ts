import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { routesAngularUniversity } from 'src/app/app-routing.module';

import * as fromAuthFeature from '../store/';
import { AuthState } from '../store/auth.reducers';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private store: Store<AuthState>,
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        console.log('[AuthGuard] :: canActivate');
        return this.store.pipe(
            select(fromAuthFeature.AuthSelectors.isLoggedIn),
            // tap((data) => console.log(data))
            tap(loggedIn => {
                if (!loggedIn) {
                    this.router.navigate([`${routesAngularUniversity.angular_university_angular_ngrx}/login`]);
                }
            })
        );
    }
}
