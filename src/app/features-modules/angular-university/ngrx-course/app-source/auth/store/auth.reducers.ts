import { Action, createReducer, MetaReducer, on } from '@ngrx/store';
import { User } from '../model/user.model';
import * as AuthActions from './auth.actions';

export const authFeatureKey = 'auth';

export interface AuthState {
  user: User;
}

export const initialAuthState: AuthState = {
  user: undefined
};

const authReducer = createReducer(
  initialAuthState,
  on(AuthActions.loginAction, (state, { user }) => {
    return {
      ...state,
      user
    };
  }),
  on(AuthActions.logoutAction, () => {
    return {
      user: undefined
    };
  }),

);

export function reducer(state: AuthState | undefined, action: Action) {
  return authReducer(state, action);
}

export const metaReducers: MetaReducer<AuthState>[] = /*!environment.production ? [] :*/[];
