import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { authActionsType } from './auth.actions';
import { AuthEffects } from './auth.effects';

fdescribe('authEffects', () => {

  let actions$: Observable<Action>;
  let effects: AuthEffects;
  let routerStub: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        provideMockActions(() => actions$),
        AuthEffects,
      ],
    });

    effects = TestBed.get(AuthEffects);
    routerStub = TestBed.get(Router);

  });

  it('AuthEffects should be created', async () => {
    expect(effects).toBeTruthy();
  });

  it('AuthEffects should save the user profile in localStorage', async () => {
    actions$ = of({
      type: authActionsType.loginAction,
      user: 'bob'
    });

    effects.login$.subscribe(() => {
      expect(localStorage.getItem('user')).not.toBeNull();
    });
  });

  it('AuthEffects shouldn\'t be in localStorage the user profile', async () => {

    spyOn(routerStub, 'navigate').and.callFake(() => Promise.resolve(true));
    actions$ = of({
      type: authActionsType.logoutAction
    });

    effects.logout$.subscribe(() => {
      expect(localStorage.getItem('user')).toBeNull();
    });
  });
});
