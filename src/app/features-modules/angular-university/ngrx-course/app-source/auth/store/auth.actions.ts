import { createAction, props } from '@ngrx/store';
import { User } from '../model/user.model';

export const authActionsType = {
  loginAction: '[Login Page] User Login',
  logoutAction: '[Top Menu] Logout'
};

export const loginAction = createAction(
  authActionsType.loginAction,
  props<{ user: User }>()
);

export const logoutAction = createAction(
  authActionsType.logoutAction
);

