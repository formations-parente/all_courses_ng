import { Store } from '@ngrx/store';

import { User } from '../model/user.model';
import { AuthState } from '../store/auth.reducers';
import { loginAction, logoutAction } from './auth.actions';

export const dispatchActionLogin = (store: Store<AuthState>, user: User) => {
    store.dispatch(loginAction({ user }));
};

export const dispatchActionLogout = (store: Store<AuthState>) => {
    store.dispatch(logoutAction());
};
