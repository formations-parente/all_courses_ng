import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { routesAngularUniversity } from 'src/app/app-routing.module';
import { AuthActions } from '.';
import { User } from '../model/user.model';

@Injectable()
export class AuthEffects {

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginAction),
    tap(action => this.saveUserToLocalStorage(action.user))
  ), { dispatch: false });

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.logoutAction),
    tap(() => {
      localStorage.removeItem('user');
      this.router.navigate([`${routesAngularUniversity.angular_university_angular_ngrx}/login`]);
    })
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private router: Router
  ) { }

  private saveUserToLocalStorage(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }
}
