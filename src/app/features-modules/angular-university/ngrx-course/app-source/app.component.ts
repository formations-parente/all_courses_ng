import { Component, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FakeService } from 'src/app/shared/services/fake.service';
import * as fromAuthFeature from './auth/store';
import { dispatchActionLogin } from './auth/store/auth.dispachers';
import { AuthState } from './auth/store/auth.reducers';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  loading = false;
  isLoggedIn$: Observable<boolean>;
  isLoggedOut$: Observable<boolean>;
  userProfileStr: string;

  constructor(
    private router: Router,
    private store: Store<AuthState>,
    private fakeService: FakeService
  ) { }

  ngOnInit() {

    this.readUserFromLocalStorage();

    this.router.events.subscribe(event => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

    // L'opérateur select permet de faire un filtre sur la duplication du valeurs
    this.isLoggedIn$ = this.store.pipe(
      select(fromAuthFeature.AuthSelectors.isLoggedIn)
    );

    this.isLoggedOut$ = this.store.pipe(
      select(fromAuthFeature.AuthSelectors.isLoggedOut)
    );

    this.store.subscribe(stateValue => console.log('Store value: ', stateValue));

    console.log('[feature AU-NGRX] AppComponent this.fakeService.getCount() =>', this.fakeService.getCount());
  }

  logout() {
    fromAuthFeature.AuthDispatchers.dispatchActionLogout(this.store);
  }


  private readUserFromLocalStorage(): void {
    // tslint:disable-next-line: no-conditional-assignment
    if ((this.userProfileStr = localStorage.getItem('user'))) {
      dispatchActionLogin(this.store, JSON.parse(this.userProfileStr));
    }
  }

}
