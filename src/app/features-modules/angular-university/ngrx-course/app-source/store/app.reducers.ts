import { AuthReducers } from '../auth/store';

export const rootReducer = {
    auth: AuthReducers.reducer
};
