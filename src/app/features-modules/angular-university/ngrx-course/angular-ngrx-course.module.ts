import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { AngularNgrxCourseRoutingModule } from './angular-ngrx-course-routing.module';
import { AuthModule } from './app-source/auth/auth.module';
import { AuthService } from './app-source/auth/auth.service';

@NgModule({
  declarations: [
    ...AngularNgrxCourseRoutingModule.components
  ],
  imports: [
    SharedModule,
    AuthModule.forRoot(),
    AngularNgrxCourseRoutingModule,
  ],
  providers: [AuthService]
})
export class AngularNgrxCourseModule { }
