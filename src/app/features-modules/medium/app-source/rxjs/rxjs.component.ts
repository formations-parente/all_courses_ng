import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, mergeMap, delay, concatMap } from 'rxjs/operators';
import { interval, forkJoin } from 'rxjs';

@Component({
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RxjsComponent implements OnInit {

  // tslint:disable-next-line: max-line-length
  // https://levelup.gitconnected.com/handle-multiple-api-requests-in-angular-using-mergemap-and-forkjoin-to-avoid-nested-subscriptions-a20fb5040d0c

  title = 'Medium about angular';
  username;
  posts;
  albums;

  constructor(
    private http: HttpClient
  ) {
    this.posts = [];
    this.albums = [];
    this.username = 'XXX';
  }

  ngOnInit(): void {

  }

  private getDataByUsingSubscribe() {
    console.time('getDataByUsingSubscribe');
    this.http.get('https://jsonplaceholder.typicode.com/users?username=Bret')
      .pipe(
        map(users => users[0]),
        tap(console.log)
      ).subscribe(user => {
        this.username = user.username;

        this.http.get(`https://jsonplaceholder.typicode.com/posts?userId=${user.id}`)
          .subscribe(
            posts => {
              this.posts = posts;
            });

        this.http.get(`https://jsonplaceholder.typicode.com/albums?userId=${user.id}`)
          .subscribe(
            albums => {
              this.albums = albums;
            });

      });
    console.timeEnd('getDataByUsingSubscribe');
  }

  loadData() {
    this.getDataByUsingSubscribe();
  }

  loadDataNg() {
    interval(1).pipe(
      concatMap(() =>
        this.http.get('https://jsonplaceholder.typicode.com/users?username=Bret')
          .pipe(
            tap(() => console.log('Another request [users]')),
            delay(1500),
            map((users: any[]) => {
              const [user] = users;
              this.username = user.username;
              return user;
            }),
            concatMap(user => this.http.get(`https://jsonplaceholder.typicode.com/albums?userId=${user.id}`).pipe(
              delay(9500),
              tap(() => console.log('Another request [albums]')),
            ))
          )
      )
    ).subscribe(
      posts => {
        this.posts = posts;
        console.log(this.posts);
      });
  }

  loadDataWithForkJoin() {
    this.http.get('https://jsonplaceholder.typicode.com/users?username=Bret')
      .pipe(
        tap(() => console.log('loadDataWithForkJoin :: request [users]')),
        map((users: any[]) => {
          const [user] = users;
          this.username = user.username;
          return user;
        }),
        mergeMap(user => {
          const posts = this.http.get(`https://jsonplaceholder.typicode.com/posts?userId=${user.id}`);
          const albums = this.http.get(`https://jsonplaceholder.typicode.com/albums?userId=${user.id}`);
          return forkJoin(posts, albums);
        })
      ).subscribe(result => {
        this.posts = result[0];
        this.albums = result[1];

        console.log(this.posts, this.albums);
      });
  }

}


