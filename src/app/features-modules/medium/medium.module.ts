import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MediumRoutingModule } from './medium-routing.module';



@NgModule({
  declarations: [
    MediumRoutingModule.components
  ],
  imports: [
    SharedModule,
    MediumRoutingModule
  ]
})
export class MediumModule { }
