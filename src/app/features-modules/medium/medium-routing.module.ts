import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app-source/app.component';
import { RxjsComponent } from './app-source/rxjs/rxjs.component';

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: 'rxjs',
                component: RxjsComponent,
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MediumRoutingModule {
    static components = [
        AppComponent,
        RxjsComponent
    ];
}
