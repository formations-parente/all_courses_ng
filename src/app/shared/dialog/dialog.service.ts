import { ApplicationRef, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector, Type } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { DialogConfig } from './dialog-config';
import { DialogInjector } from './dialog-injector';
import { DialogRef } from './dialog-ref';
import { DialogComponent } from './dialog.component';
import { DialogModule } from './dialog.module';

@Injectable({
  providedIn: DialogModule,
})
export class DialogService {
  dialogComponentRef: ComponentRef<DialogComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector
  ) { }

  public open(componentType: Type<any>, config: DialogConfig) {
    const dialogRef = this.appendDialogComponentToBody(config);

    this.dialogComponentRef.instance.childComponentType = componentType;
    return dialogRef;
  }

  private appendDialogComponentToBody(config: DialogConfig): DialogRef {
    /**
     * Create a map with the config and DialogRef
     */
    const map = new WeakMap();
    map.set(DialogConfig, config);


    /**
     * add the DialogRef to dependency injection
     */
    const dialogRef = new DialogRef();
    map.set(DialogRef, dialogRef);

    // we want to know when somebody called the close mehtod
    dialogRef.afterClosed.pipe(
      tap(this.removeDialogComponentFromBody),
      take(1)
    ).subscribe();

    /**
     * ComponentFactoryResolver provided by angular. This service is using the type of the component to look up the factory.
     */
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DialogComponent);

    /**
     * We are passing in the injector we requested in the constructor.
     * This enables the dynamic component to make use of dependency injection itself.
     */
    const componentRef = componentFactory.create(new DialogInjector(this.injector, map));

    /**
     * Afterward, we need to attach the new component to the angular component tree (which is separate from the DOM).
     */
    this.appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(domElem);

    this.dialogComponentRef = componentRef;

    this.dialogComponentRef.instance.onClose.pipe(
      tap(this.removeDialogComponentFromBody)
    ).subscribe();

    return dialogRef;
  }

  removeDialogComponentFromBody = () => {
    this.appRef.detachView(this.dialogComponentRef.hostView);
    this.dialogComponentRef.destroy();
  }
}
