import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ExampleComponent } from './components/example/example.component';
import { DialogComponent } from './dialog.component';
import { InsertionDirective } from './insertion.directive';

@NgModule({
  declarations: [DialogComponent, InsertionDirective, ExampleComponent],
  imports: [
    CommonModule
  ],
  entryComponents: [DialogComponent, ExampleComponent]
})
export class DialogModule { }
