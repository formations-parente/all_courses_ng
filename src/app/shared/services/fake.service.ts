import { Injectable } from '@angular/core';

@Injectable()
export class FakeService {

    private count = 0;

    constructor() {
        this.add();
    }

    add() {
        this.count++;
    }

    getCount() {
        return this.count;
    }
}
