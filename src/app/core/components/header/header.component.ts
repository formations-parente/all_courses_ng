import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/router';
import { routesPluralsight, routesAngularUniversity, routesMedium } from 'src/app/app-routing.module';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public pluralSightAngularRouting = `/${routesPluralsight.pluralsight_angular_routing}`;
  public angularUniversityAngularTesting = `/${routesAngularUniversity.angular_university_angular_testing}`;
  public angularUniversityAngularNgrx = `/${routesAngularUniversity.angular_university_angular_ngrx}`;
  public mediumAngularRxjs = `/${routesMedium.medium_angular}/rxjs`;

}
